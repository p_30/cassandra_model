USE [p_30]
GO

/****** Object:  Table [dbo].[SURWAY_DATA_BY_AGE_HIST]    Script Date: 05.07.2020 22:45:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[SURWAY_DATA_BY_AGE_HIST](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[id_uuid] [nvarchar](40) NOT NULL,
	[age] [int] NOT NULL,
	[count_rating] [int] NOT NULL,
	[avg_rating] [float] NOT NULL,
	[min_rating] [int] NOT NULL,
	[max_rating] [int] NOT NULL,
	[count_amount] [int] NOT NULL,
	[avg_amount] [float] NOT NULL,
	[min_amount] [int] NOT NULL,
	[max_amount] [int] NOT NULL,
	[date] [datetime] NOT NULL,
 CONSTRAINT [PK_SURWAY_DATA_BY_AGE_HIST] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

